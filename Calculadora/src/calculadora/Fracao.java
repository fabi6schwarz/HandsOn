/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author fabe6
 */
public class Fracao {
    int numerador;
    int denominador;
    
    public Fracao(){
    }
    public Fracao(int numerador, int denominador){
        this.numerador=numerador;
        this.denominador=denominador;
    }

    public int getNumerador() {
        return this.numerador;
    }

    public int getDenominador() {
        return this.denominador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public void setDenominador(int denominador) {
        this.denominador = denominador;
    }
    
}
