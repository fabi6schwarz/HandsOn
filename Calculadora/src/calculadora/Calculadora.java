/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author fabe6
 */
public class Calculadora {
    public Calculadora(){
    }
    
    public Fracao soma(Fracao f1, Fracao f2){
        int n1=f1.getNumerador();
        int d1=f1.getDenominador();
        int n2=f2.getNumerador();
        int d2=f2.getDenominador();
        /*int denominadorComum=denominadorComum(d1,d2);
        int n1Soma=(n1/d1)*denominadorComum;
        int n2Soma=(n2/d2)*denominadorComum;*/
        //como não foi pedida a fracao mais simplificada possivel,
        //para evitar numeros quebrados no numerador,
        //decidiu-se por utilizar somente multiplicacoes
        int[] valoresDeSoma = getValoresDeSoma(n1,d1,n2,d2);
        int somaFinal = valoresDeSoma[0]+valoresDeSoma[1];
        Fracao fracaoFinal = new Fracao(somaFinal,valoresDeSoma[2]);
        return fracaoFinal;
    }
    public Fracao subtracao(Fracao f1, Fracao f2){
        int n1=f1.getNumerador();
        int d1=f1.getDenominador();
        int n2=f2.getNumerador();
        int d2=f2.getDenominador();
        int[] valoresDeSoma = getValoresDeSoma(n1,d1,n2,d2);
        int somaFinal = valoresDeSoma[0]-valoresDeSoma[1];
        Fracao fracaoFinal = new Fracao(somaFinal,valoresDeSoma[2]);
        return fracaoFinal;
    }
    public Fracao multiplicacao(Fracao f1, Fracao f2){
        int n1=f1.getNumerador();
        int d1=f1.getDenominador();
        int n2=f2.getDenominador();
        int d2=f2.getDenominador();
        
        int multNomi=n1*n2;
        int multDenomi=d1*d2;
        Fracao fracaoFinal = new Fracao(multNomi,multDenomi);
        return fracaoFinal;
    }
    public Fracao divisao(Fracao f1, Fracao f2){
        int n1=f1.getNumerador();
        int d1=f1.getDenominador();
        int n2=f2.getDenominador();
        int d2=f2.getDenominador();
        
        int multNomi=n1*d2;
        int multDenomi=d1*n2;
        Fracao fracaoFinal = new Fracao(multNomi,multDenomi);
        return fracaoFinal;
    }
    
    private int[] getValoresDeSoma(int n1,int d1,int n2,int d2){
        int n1temp=n1;
        int n2temp=n2;
        int denominador=d1;
        if(d1!=d2){
            denominador=d1*d2;
            n1temp=n1*d2;
            n2temp=n2*d1;
        }
        int[] valoresDeSoma={n1temp,n2temp,denominador};
        return valoresDeSoma;
    }
    private int denominadorComum(int d1, int d2){
        return (d1*d2)/maximoDivisorComum(d1,d2);
    }
    private int maximoDivisorComum(int d1, int d2){
        int resto;
        if(d2>d1){
            int temp=d1;
            d1=d2;
            d2=temp;
        }
        do {
        resto = d1 % d2;
        d1 = d2;
        d2 = resto;
        } while (resto != 0);
        return d1;
    }
}
