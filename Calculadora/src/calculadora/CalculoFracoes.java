/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author fabe6
 */
import calculadora.Calculadora;
import calculadora.Fracao;
public class CalculoFracoes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fracao f1 = new Fracao(4,3);
        Fracao f2 = new Fracao(2,3);
        
        Calculadora calculadora = new Calculadora();
        Fracao soma=calculadora.soma(f1, f2);
        System.out.println(soma.getNumerador()+"/"+soma.getDenominador());
        
    }
    
}
