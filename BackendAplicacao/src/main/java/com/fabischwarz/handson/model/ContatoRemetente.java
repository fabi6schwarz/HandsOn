package com.fabischwarz.handson.model;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import org.json.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.FileReader;
import java.io.FileWriter;

@RestController
@CrossOrigin

public class ContatoRemetente {
    private String nome;
    private String telefone;
    private String cpf;
    private String endereco;
    private String email;

    public ContatoRemetente() {
    }
    
    
    public ContatoRemetente(String nome,String telefone,String cpf,String endereco,String email){
        this.nome= nome;
        this.telefone=telefone;
        this.cpf=cpf;
        this.endereco=endereco;
        this.email=email;
    }
    public String getRemetentesPath(){
        Path currentPath = Paths.get(System.getProperty("user.dir"));
        Path filePath = Paths.get(currentPath.toString(), "src","main","java","com","fabischwarz","handson","remetentes.json");
        return filePath.toString();
    }
    public Object getRemetentes(){
        JSONParser parser = new JSONParser();
        Object remetentesJSON = new Object();
        try (FileReader reader = new FileReader(getRemetentesPath())){

            remetentesJSON = parser.parse(reader);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return remetentesJSON;
    }
    public boolean delete(String email){
        JSONArray remetentesJSON = (JSONArray) getRemetentes();
        //remetentesJSON.remove(procuraIndiceEmail(email));
        //JSONObject remetentesJSON = (JSONObject) getRemetentes();
        //for(int i=0; i<remetentesJSON.size();i++){
        //    ((JSONObject)remetentesJSON.get(i)).remove("email", email);
        //}
        //ContatoRemetente cr = new ContatoRemetente();
        //cr.save(remetentesJSON);
            return true;
    }
    public Object getByEmail(String email){
        JSONArray remetentesJSON = (JSONArray) getRemetentes();
        return remetentesJSON.get(0);
    }
/*
    private int procuraIndiceEmail(String email){
        JSONObject remetentesJSON = (JSONObject) getRemetentes();
            for(int index = 0; index < remetentesJSON.; index++) {
                JSONObject jsonObject = remetentesJSON.removeIf("email"=email);
                if(jsonObject.email.equals(email)) {
                    return index;
                } 
            }
            return -1; //it wasn't found at all
        }
    }*/
    

    public void save(JSONArray remetentesJSON){
        
        try (FileWriter file = new FileWriter(getRemetentesPath())) {
 
            file.write(remetentesJSON.toString());
            file.flush();
 
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void save(JSONObject remetentesJSON){
        
        try (FileWriter file = new FileWriter(getRemetentesPath())) {
 
            file.write(remetentesJSON.toString());
            file.flush();
 
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void saveMerge(){
        JSONArray remetentesJSON = (JSONArray) getRemetentes();
        
        JSONObject novo = new JSONObject();
        novo.put("nome", this.nome);
        novo.put("telefone", this.telefone);
        novo.put("cpf", this.cpf);
        novo.put("endereco", this.endereco);
        novo.put("email", this.email);

        remetentesJSON.add(novo);

        try (FileWriter file = new FileWriter(getRemetentesPath())) {
 
            file.write(remetentesJSON.toString());
            file.flush();
 
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String getNome(){
        return this.nome;
    }
    public String getTelefone(){
        return this.telefone;
    }
    public String getCpf(){
        return this.cpf;
    }
    public String getEndereco(){
        return this.endereco;
    }
    public String getEmail(){
        return this.email;
    }
    
}