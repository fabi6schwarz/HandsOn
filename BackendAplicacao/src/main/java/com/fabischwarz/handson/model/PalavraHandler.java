package com.fabischwarz.handson.model;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class PalavraHandler{
    private String palavra;

    public PalavraHandler(String palavra){
        this.palavra=palavra;
    }
    public PalavraHandler(){
    }
    public int getTamanho(){
        return this.palavra.length();
    }
    public String getMaiuscula(){
        return this.palavra.toUpperCase();
    }

    public String getVogais(){
        String str=this.palavra;
        String vogais = "";
            for(int i=0; i<str.length(); i++) {
                if(str.charAt(i) == 'a'|| str.charAt(i) == 'e'|| str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') {
                    vogais=vogais+str.charAt(i);
                }
            }
        return vogais;
    }
    public String getConsoantes(){
        String str=this.palavra;
        String consoantes = "";
            for(int i=0; i<str.length(); i++) {
                if(str.charAt(i) == 'a'|| str.charAt(i) == 'e'|| str.charAt(i) == 'i' || str.charAt(i) == 'o' || str.charAt(i) == 'u') {
                }else{
                    consoantes=consoantes+str.charAt(i);
                }
            }
        return consoantes;
    }

}