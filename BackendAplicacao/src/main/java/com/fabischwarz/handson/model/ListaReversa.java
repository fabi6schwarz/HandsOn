package com.fabischwarz.handson.model;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class ListaReversa{
    private String lista;

    public ListaReversa(String lista){
        this.lista=listaToString(reverterLista(stringToArray(lista)));
    }
    public ListaReversa(){
    }
    public String getLista(){
        return this.lista;
    }

    private String[] stringToArray(String lista){
        lista=lista.replace("{", "");
        lista=lista.replace("}", "");
        String[] listaArray = lista.split(",");
        return listaArray;
    }
    private String[] reverterLista(String[] listaArray){
        String[] listaReversa = new String[listaArray.length];
        int j=listaArray.length-1;

        for (int i = 0; i < listaArray.length; i++){
            listaArray[i]=listaArray[i].trim();
            listaReversa[j]=listaArray[i];
            j--;
            System.out.println(listaArray[i]);
        }
        return listaReversa;
    }
    private String listaToString(String[] listaArray){
        return "{"+String.join(", ",listaArray)+"}";
    }
}