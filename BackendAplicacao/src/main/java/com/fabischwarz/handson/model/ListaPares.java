package com.fabischwarz.handson.model;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class ListaPares{
    private String lista;

    public ListaPares(String lista){
        this.lista="{"+getParesFromArrayAsString(arrayStrToArrayInt(stringToArray(lista)))+"}";
    }
    public ListaPares(){
    }
    public String getLista(){
        return this.lista;
    }

    private String[] stringToArray(String lista){
        lista=lista.replace("{", "");
        lista=lista.replace("}", "");
        String[] listaArray = lista.split(",");
        return listaArray;
    }
    
    private int[] arrayStrToArrayInt(String[] listaArray){
        int[] listaInt = new int[listaArray.length];

        for (int i = 0; i < listaArray.length; i++){
            listaInt[i]=Integer.parseInt(listaArray[i].trim());
        }
        return listaInt;
    }

    private String getParesFromArrayAsString(int[] listaInt){
        ArrayList<Integer> listaPares = new ArrayList();
        for (int i = 0; i < listaInt.length; i++){
            if(listaInt[i] % 2 == 0){
                listaPares.add(listaInt[i]);
            }
        }
        return listaPares.toString().replace("[", "").replace("]","");
    }

}