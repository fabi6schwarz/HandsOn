package com.fabischwarz.handson.model;


import org.springframework.web.bind.annotation.RestController;

@RestController

public class Nome{
    private String nome;

    public Nome(String nome){
        this.nome=nome;
    }
    public Nome(){
    }

    public String getNomeBibliografico(){
        String nomeCompleto = this.nome;
        int ultimoEspaco=nomeCompleto.lastIndexOf(" ");
        String sobrenome=nomeCompleto.substring(ultimoEspaco);
        String nome = nomeCompleto.substring(0, ultimoEspaco);
        return sobrenome+", "+nome;
    }

}