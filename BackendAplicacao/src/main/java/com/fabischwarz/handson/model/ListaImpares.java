package com.fabischwarz.handson.model;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class ListaImpares{
    private String lista;

    public ListaImpares(String lista){
        this.lista="{"+getImparesFromArrayAsString(arrayStrToArrayInt(stringToArray(lista)))+"}";
    }
    public ListaImpares(){
    }
    public String getLista(){
        return this.lista;
    }

    private String[] stringToArray(String lista){
        lista=lista.replace("{", "");
        lista=lista.replace("}", "");
        String[] listaArray = lista.split(",");
        return listaArray;
    }
    
    private int[] arrayStrToArrayInt(String[] listaArray){
        int[] listaInt = new int[listaArray.length];

        for (int i = 0; i < listaArray.length; i++){
            listaInt[i]=Integer.parseInt(listaArray[i].trim());
        }
        return listaInt;
    }

    private String getImparesFromArrayAsString(int[] listaInt){
        ArrayList<Integer> listaImpares = new ArrayList();
        for (int i = 0; i < listaInt.length; i++){
            if(listaInt[i] % 2 == 1){
                listaImpares.add(listaInt[i]);
            }
        }
        return listaImpares.toString().replace("[", "").replace("]","");
    }

}