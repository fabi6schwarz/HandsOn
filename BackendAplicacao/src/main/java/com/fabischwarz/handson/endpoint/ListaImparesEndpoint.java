package com.fabischwarz.handson.endpoint;

import com.fabischwarz.handson.model.ListaImpares;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/imprimirImpares")
public class ListaImparesEndpoint{

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> ListarImpares(@RequestParam("lista") String query) {
        ListaImpares listaImpares = new ListaImpares(query);
        return new ResponseEntity<>("HTTP 200 OK "+listaImpares.getLista(), HttpStatus.OK);
    }
}

