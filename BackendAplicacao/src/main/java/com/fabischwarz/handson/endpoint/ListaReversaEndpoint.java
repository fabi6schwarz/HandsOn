package com.fabischwarz.handson.endpoint;

import com.fabischwarz.handson.model.ListaReversa;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/listaReversa")
public class ListaReversaEndpoint{

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> ListarReverso(@RequestParam("lista") String query) {
        ListaReversa listaReversa = new ListaReversa(query);
        return new ResponseEntity<>("HTTP 200 OK "+listaReversa.getLista(), HttpStatus.OK);
    }
}

