package com.fabischwarz.handson.endpoint;

import com.fabischwarz.handson.model.PalavraHandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class PalavraHandlerEndpoint{
    
    @RequestMapping(path="/tamanho",method = RequestMethod.GET)
    public ResponseEntity<?> tamanho(@RequestParam("palavra") String query) {
        PalavraHandler palavra = new PalavraHandler(query);
        return new ResponseEntity<>("HTTP 200 OK "+"Tamanho = "+palavra.getTamanho(), HttpStatus.OK);
    }

    @RequestMapping(path="/maiusculas",method = RequestMethod.GET)
    public ResponseEntity<?> maiusculas(@RequestParam("palavra") String query) {
        PalavraHandler palavra = new PalavraHandler(query);
        return new ResponseEntity<>("HTTP 200 OK "+palavra.getMaiuscula(), HttpStatus.OK);
    }

    @RequestMapping(path="/vogais",method = RequestMethod.GET)
    public ResponseEntity<?> vogais(@RequestParam("palavra") String query) {
        PalavraHandler palavra = new PalavraHandler(query);
        return new ResponseEntity<>("HTTP 200 OK "+palavra.getVogais(), HttpStatus.OK);
    }

    @RequestMapping(path="/consoantes",method = RequestMethod.GET)
    public ResponseEntity<?> consoantes(@RequestParam("palavra") String query) {
        PalavraHandler palavra = new PalavraHandler(query);
        return new ResponseEntity<>("HTTP 200 OK "+palavra.getConsoantes(), HttpStatus.OK);
    }

}

