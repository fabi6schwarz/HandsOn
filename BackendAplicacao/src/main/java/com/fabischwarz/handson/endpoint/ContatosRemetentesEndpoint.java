package com.fabischwarz.handson.endpoint;

import java.util.Map;

import com.fabischwarz.handson.model.ContatoRemetente;

import org.json.simple.JSONArray;
import org.apache.catalina.filters.CorsFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@RestController
@ResponseBody
@CrossOrigin


public class ContatosRemetentesEndpoint{
    @Configuration
    public class CorsConfiguration implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedMethods("*").allowedOrigins("*").allowedHeaders("*");
    }
}
    @RequestMapping(path="/contatosRemetentes",method = RequestMethod.GET)
    public ResponseEntity<?> remetentes() {
        ContatoRemetente contatos = new ContatoRemetente();
        return new ResponseEntity<>(contatos.getRemetentes(), HttpStatus.OK);
    }


    @PostMapping(path="/contatosRemetentes")
    public void create(@RequestBody Map<String, String> body){
        String nome = body.get("nome");
        String telefone = body.get("telefone");
        String cpf = body.get("cpf");
        String endereco = body.get("endereco");
        String email = body.get("email");
        ContatoRemetente contatoRemetente = new ContatoRemetente(nome, telefone, cpf,endereco,email);
        contatoRemetente.saveMerge();
    }

    /*@RequestMapping(value = "/{email}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String email) {
        ContatoRemetente contatoRemetente = new ContatoRemetente();
        contatoRemetente.delete(email);
    }*/

    @DeleteMapping(value = "/{email}")
    public ResponseEntity<?> delete(@PathVariable String email) {
        ContatoRemetente contatoRemetente = new ContatoRemetente();
        var isRemoved = contatoRemetente.delete(email);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(email, HttpStatus.OK);
    }

    

}