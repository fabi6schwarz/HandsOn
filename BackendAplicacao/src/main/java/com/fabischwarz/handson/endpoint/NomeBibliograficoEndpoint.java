package com.fabischwarz.handson.endpoint;

import com.fabischwarz.handson.model.Nome;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
public class NomeBibliograficoEndpoint{
    
    @RequestMapping(path="/nomeBibliografico",method = RequestMethod.GET)
    public ResponseEntity<?> tamanho(@RequestParam("nome") String query) {
        Nome nome = new Nome(query);
        return new ResponseEntity<>("HTTP 200 OK "+nome.getNomeBibliografico(), HttpStatus.OK);
    }


}

