package com.fabischwarz.handson.endpoint;

import com.fabischwarz.handson.model.ListaPares;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseBody
@RequestMapping("/imprimirPares")
public class ListaParesEndpoint{

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> ListarPares(@RequestParam("lista") String query) {
        ListaPares listaPares = new ListaPares(query);
        return new ResponseEntity<>("HTTP 200 OK "+listaPares.getLista(), HttpStatus.OK);
    }
}

