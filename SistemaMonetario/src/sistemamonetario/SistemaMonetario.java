/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemamonetario;

/**
 *
 * @author fabe6
 */

public class SistemaMonetario {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int valorDeSaque=312; //inserir valor de saque aqui
        
        Saque saque = new Saque(valorDeSaque);
        System.out.println("Numero de notas de Cinco: "+saque.getNumeroDeNotasDeCinco());
        System.out.println("Numero de notas de Tres: "+saque.getNumeroDeNotasDeTres());
        System.out.println("Valor Solicitado: "+valorDeSaque);
        System.out.println("Valor Final Sacado: "+saque.getValorSacado());
    }
    
}
