/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemamonetario;

/**
 *
 * @author fabe6
 */
public class Saque{
    int valorSaque;
    int notasDeTres;
    int notasDeCinco;

    public Saque(int valorSaque){
        this.valorSaque=valorSaque;
        setNotas(valorSaque);
    }

    public void setNotas(int valorSaque){
        int notasDeCinco=0;
        int notasDeTres=0;
            if(valorSaque%5==0){
                notasDeCinco=(valorSaque/5);
                notasDeTres=0;
            }
            if(valorSaque%5==1){
                notasDeCinco=(valorSaque/5)-1;
                notasDeTres=2;
            }
            if(valorSaque%5==2){
                notasDeCinco=(valorSaque/5)-2 ;
                notasDeTres=4;
            }
            if(valorSaque%5==3){
                notasDeCinco=(valorSaque/5);
                notasDeTres=1;
            }
            if(valorSaque%5==4){
                notasDeCinco=(valorSaque/5)-1;
                notasDeTres=3;
            }
        this.notasDeCinco=notasDeCinco;
        this.notasDeTres=notasDeTres;
    }

    public int getNumeroDeNotasDeTres(){
        return this.notasDeTres;
    }
    
    public int getValorDeSaque(){
        return this.valorSaque;
    }
    
    public int getNumeroDeNotasDeCinco(){
        return this.notasDeCinco;
    }

    public int getValorSacado() {
        return (this.notasDeCinco*5) + (this.notasDeTres*3);
    }
}
